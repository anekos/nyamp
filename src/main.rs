
extern crate iron;
#[macro_use]
extern crate router;
#[macro_use]
extern crate lazy_static;
extern crate argparse;
extern crate url;
extern crate curl;

use std::io::Read;
use std::net::{SocketAddrV4, Ipv4Addr};

use argparse::{ArgumentParser, Store};
use iron::headers::ContentType;
use iron::mime::{Mime, TopLevel, SubLevel};
use iron::prelude::*;
use iron::status;
use url::percent_encoding::percent_decode;


fn handler_jump(request: &mut Request) -> Result<Response, IronError> {
    let ref url = request.url;

    let path = url.path();
    let kind = path.get(0).unwrap();

    let jump_to = {
        let segments: Vec<&str> = url.path().iter().map(|it| it.to_owned()).skip(1).collect();
        let joined: String = segments.join("/");
        let mut buffer = percent_decode(joined.as_bytes()).decode_utf8().unwrap().into_owned();
        if let Some(query) = url.query() {
            buffer.push_str(&format!("?{}", query));
        }
        buffer
    };

    match *kind {
        "jump" => {
            println!("jump: {}", jump_to);

            let html = format!("<html>\
  <head>\
  <title>location.href</title>\
  </head>\
  <body>\
    <input id=\"url\" type=\"hidden\" value=\"{}\" />\
    <script language=\"JavaScript\">\
      location.href = document.getElementById('url').getAttribute('value');\
      </script>\
  </body>\
</html>", jump_to);

            println!("jump_to: {}", jump_to);

            let mut response = Response::with((status::Ok, html));
            response.headers.set(ContentType(Mime(TopLevel::Text, SubLevel::Html, vec![])));
            Ok(response)
        }
        "proxy" => {
            use curl::easy::Easy;
            println!("proxy: {}", jump_to);

            let mut buf: Vec<u8> = vec![];
            let mut content_type: Option<String> = None;
            let mut easy = Easy::new();
            easy.ssl_verify_host(false); // FIXME
            easy.ssl_verify_peer(false); // FIXME
            easy.follow_location(true);
            {
                if let Err(e) = easy.url(&jump_to) {
                    println!("Error");
                    return Ok(Response::with((status::NotFound, format!("Not found: {}", e))));
                }

                let mut xfer = easy.transfer();
                xfer.write_function(|data| {
                    buf.extend_from_slice(data);
                    Ok(data.len())
                }).unwrap();
                xfer.header_function(|header| {
                    let header = std::str::from_utf8(header).unwrap().to_string();
                    if let Some(sep) = header.find(':') {
                        let name = header[0..sep].to_lowercase();
                        let value = header[sep + 1..].trim();
                        if name == "content-type" {
                            content_type = Some(value.to_owned());
                        }
                    }
                    true
                }).unwrap();
                xfer.perform().unwrap();
            }
            println!("ContentType: {:?}", content_type);
            println!("Size: {}", buf.len());
            let mut response = Response::with((status::Ok, buf));
            if let Some(content_type) = content_type {
                response.headers.set_raw("content-type", vec![content_type.into_bytes()]);
            }
            Ok(response)
        }
        _ => {
            let html = "NOT IMPLEMENTED".to_owned();
            let mut response = Response::with((status::Ok, html));
            response.headers.set(ContentType(Mime(TopLevel::Text, SubLevel::Html, vec![])));
            Ok(response)
        }
    }
}


fn main() {
    let mut port = 3000;

    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut port).add_option(&["-p", "--port"], Store, "Port number");
        ap.parse_args_or_exit();
    }

    {
        let host = Ipv4Addr::new(0, 0, 0, 0);
        match Iron::new(handler_jump).http(SocketAddrV4::new(host, port)) {
            Ok(_) => println!("Launched with port {}", port),
            Err(err) => println!("{}", err)
        }
    }

}
